def separate(sen):
    res = []
    for c in sen[:]:
        if c in ('ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
            res.append(' ')
            res.append(c)
        else:
            res.append(c)
    return ''.join(res)

sen ="camelCaseIsAwesome"
print(separate(sen))