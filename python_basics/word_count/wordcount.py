import sys


def words_spliter(text):
    text = text.lower()
    i = 0
    while(i<128):
        if i==32:
            i=33
        elif i==97:
            i=123
        text=text.replace(chr(i), chr(32))
        i+=1
    return text.split()


def words_count(words_list):
    dic={}
    for word in words_list:
        if word in dic:
            dic[word]= dic.get(word) + 1
        else:
            dic[word] = 1
    return dic


def print_words(filename):
    text_file=open(filename, 'r')
    words_list = words_spliter(text_file.read())
    dictionary = words_count(words_list)
    for key in sorted(dictionary.keys()):
        print(key + " " , dictionary[key])

    text_file.close()


def print_top(filename):
    text_file = open(filename, 'r')
    words_list = words_spliter(text_file.read())
    dictionary = words_count(words_list)
    sort_dict = sorted(dictionary.items(), key=lambda x: x[1], reverse=True)
    for item in sort_dict[:20]:
        print(item[0] , " " , item[1])

    text_file.close()


def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | --topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()
