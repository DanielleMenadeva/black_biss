#include <stdio.h>
#include <stdlib.h>



void hanoi_towers(int n, char from, char by, char to) {
    if (n < 1)
        return;
    hanoi_towers(n - 1, from, to, by);
    printf("Move disk %d from %c to %c\n", n, from, to);
    hanoi_towers(n - 1, by, from, to);
}


void hanoi(int n) {
    hanoi_towers(n, 'A' , 'B', 'C');
}

int main(int argc, char* argv[]) {
    if (argc < 2)
    {
        printf("missing number parameter.\n");
        return EXIT_FAILURE;
    }

    int n = atoi(argv[1]);
    hanoi(n);
    return 0;
}
