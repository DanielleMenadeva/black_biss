#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void interpret(char* input , char* ptr) {
    char ch;
    size_t i;
    size_t loop;

    for (i = 0; input[i] != 0; i++) {
        ch = input[i];
        if (ch == '>') {
            ++ptr;
        }
        else if (ch == '<') {
            --ptr;
        }
        else if (ch == '+') {
            ++* ptr;
        }
        else if (ch == '-') {
            --* ptr;
        }
        else if (ch == '.') {
            putchar(*ptr);
        }
       
        else if (ch == '[') {
            continue;
        }
        else if (ch == ']' && *ptr) {
            loop = 1;
            while (loop > 0) {
                ch = input[--i];
                if (ch == '[') {
                    loop--;
                }
                else if (ch == ']') {
                    loop++;
                }
            }
        }
    }
}


int main(int argc, char* argv[]) {
    FILE* fi;
    unsigned char tape[1000] = { 0 };
    unsigned char* ptr = tape;
    // you can add more initial checks
    if (argc < 2)
    {
        printf("missing file_path parameter.\n");
        return EXIT_FAILURE;
    }
    fi = fopen(argv[1], "r");
    char* fstr[1000];
    fgets(fstr, 1000, fi); 
    interpret(fstr, ptr);
    return 0;
}