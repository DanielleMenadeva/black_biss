#include <stdio.h>
#include <stdlib.h>


int main(int  argc, char* argv[]) {
	if (argc < 2)
	{
		printf("missing number parameter.\n");
		return EXIT_FAILURE;
	}

	int x = atoi(argv[1]), y = atoi(argv[2]),temp;
	if (x<y)
	{
		temp = x;
		x = y;
		y = temp;
	}
	while (y!=0)
	{
		printf("%d", y);
		temp = x - y;
		if (y > temp) {

			x = y;
			y = temp;
		}
		else {
			x = temp;
		}
	}
	
	return 0;
}