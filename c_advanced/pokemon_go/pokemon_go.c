#include <stdio.h>
struct pokemon {
    int attack;
    int defense;
    int effectiveness;//1-water, 2-fire, 3-air, 4-earth
    char* name;
};

int effectiveness_calc(int pok1_effect, int pok2_effect) {
    if (pok1_effect == pok2_effect || pok1_effect % 2 == pok2_effect % 2){
        return 2;
    }
    if ((pok1_effect==4 && pok2_effect == 1) || (pok1_effect < pok2_effect && pok1_effect != 1 && pok2_effect != 4)){
        return 3;
    }
    if ((pok1_effect == 1 && pok2_effect == 4) || (pok1_effect > pok2_effect && pok1_effect != 4 && pok2_effect != 1)) {
        return 1;
    }
    else
    {
        return 0;
    }
}

int main() {
    struct pokemon* pok1 = (struct pokemon*)malloc(sizeof(struct pokemon));
    struct pokemon* pok2 = (struct pokemon*)malloc(sizeof(struct pokemon));
    pok1->name = "pokemon1";
    pok1->attack = 49;
    pok1->defense = 35;
    pok1->effectiveness = 3; 
    pok1->name = "pokemon2";
    pok1->attack = 54;
    pok1->defense = 63;
    pok1->effectiveness = 2;


    if ((50 * (pok1->attack / pok2->defense) * effectiveness_calc(pok1->effectiveness, pok2->effectiveness))> (50 * (pok2->attack / pok1->defense) * effectiveness_calc(pok2->effectiveness, pok1->effectiveness)))
    {
        printf("pokemon1 won");
    }
    if ((50 * (pok1->attack / pok2->defense) * effectiveness_calc(pok1->effectiveness, pok2->effectiveness)) < (50 * (pok2->attack / pok1->defense) * effectiveness_calc(pok2->effectiveness, pok1->effectiveness)))
    {
        printf("pokemon2 won");
    }
    if ((50 * (pok1->attack / pok2->defense) * effectiveness_calc(pok1->effectiveness, pok2->effectiveness)) == (50 * (pok2->attack / pok1->defense) * effectiveness_calc(pok2->effectiveness, pok1->effectiveness)))
    {
        printf("it's a draw");
    }

    return 0;
}