"""
Black Biss - Advance Python

Write a function that read one line from the user with many potentional passwords seperated by comma ","
and print out only the valid password - by the following rules:
    * contain letter from each of the characters-sets:
        lower-case english, upper-case english, decimal digit,
        special characters *&@#$%^
    * dosn't contain other symbols (ignore white-spaces)
    * length between 6 and 12

"""


def check(include, pas):
    for char in pas:
        if char in include:
            return True
    return False

def validate_passwords(passwords):
    ans=[]
    for pas in passwords.split(','):
        if(len(pas)>6) and (len(pas)<12):
            if(check("@#$%^&*", pas)):
                if (check("0123456789", pas)):
                    if (check("ABCDEFGHIJKLMNOPQRSTUVWXYZ", pas)):
                        if (check("abcdefghijklmnopqrstuvwxyz", pas)):
                            ans.append(pas)
    return ans




# small test to check it's work.
if __name__ == '__main__':
    print(validate_passwords("ABd1234@1, F1#, 2w3E*, 2We3345"))
