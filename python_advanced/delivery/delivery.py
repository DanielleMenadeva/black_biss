"""
Black Biss - Advance Python

Write a function that get array of string, each represent a row in 2-dim map, when:
    'X' is start / destination point
    '-' is road right-left
    '|' is road up-down
    '+' is a turn in the road, can be any turn (which isn't continue straight)
    path = ["           ",
            "X-----+    "
            "      |    "
            "      +---X",
            "           "]

    # Note: this grid is only valid when starting on the right-hand X, but still considered valid
    path = ["                      ",
            "   +-------+          ",
            "   |      +++---+     ",
            "X--+      +-+   X     "]
    nevigate_validate(path)  # ---> True
"""


def nevigate(path, i, j, dire, side):
    if(i < 0) or (j < 0) or (j > len(path[i]) - 1) or (i > len(path) - 1):
        return False
    if (path[i][j] == "X"):
        return True
    # right
    if (dire and side):
        if(path[i][j] == "-"):
            if (j < len(path[i]) - 1):
                if (path[i][j + 1] == "+") or (path[i][j + 1] == "-") or (path[i][j + 1] == "X"):
                    return nevigate(path, i, j + 1, True, True)
        if(path[i][j] == "+"):
            if (i > 0):
                if (path[i - 1][j] == "+") or (path[i - 1][j] == "|") or (path[i - 1][j] == "X"):
                    if(nevigate(path, i - 1, j, False, False)):
                        return True
            if (i < len(path) - 1):
                if (path[i + 1][j] == "+") or (path[i + 1][j] == "|") or (path[i + 1][j] == "X"):
                    return nevigate(path, i + 1, j, False, True)
        return False
    # left
    if (dire and not side):
        if (path[i][j] == "-"):
            if (j > 0):
                if (path[i][j - 1] == "+") or (path[i][j - 1] == "-") or (path[i][j - 1] == "X"):
                    return nevigate(path, i, j - 1, True, False)
        if (path[i][j] == "+"):
            if (i > 0):
                if (path[i - 1][j] == "+") or (path[i - 1][j] == "|") or (path[i - 1][j] == "X"):
                    if nevigate(path, i - 1, j, False, False):
                        return True
            if (i < len(path) - 1):
                if (path[i + 1][j] == "+") or (path[i + 1][j] == "|") or (path[i + 1][j] == "X"):
                    return nevigate(path, i + 1, j, False, True)
        return False
    # up
    if (not dire and not side):
        if (path[i][j] == "|"):
            if (i > 0):
                if (path[i - 1][j] == "+") or (path[i - 1][j] == "|") or (path[i - 1][j] == "X"):
                    return nevigate(path, i - 1, j, False, False)
        if (path[i][j] == "+"):
            if (j > 0):
                if (path[i][j - 1] == "+") or (path[i][j - 1] == "-") or (path[i][j - 1] == "X"):
                    if nevigate(path, i, j - 1, True, False):
                        return True
            if (j < len(path[i]) - 1):
                if (path[i][j + 1] == "+") or (path[i][j + 1] == "-") or (path[i][j + 1] == "X"):
                    return nevigate(path, i, j + 1, True, True)
        return False
    # down
    if (not dire and side):
        if (path[i][j] == "|"):
            if (i < len(path) - 1):
                if (path[i + 1][j] == "+") or (path[i + 1][j] == "|") or (path[i + 1][j] == "X"):
                    return nevigate(path, i + 1, j, False, True)
        if (path[i][j] == "+"):
            if (j > 0):
                if (path[i][j - 1] == "+") or (path[i][j - 1] == "-") or (path[i][j - 1] == "X"):
                    if(nevigate(path, i, j - 1, True, False)):
                        return True
            if (j < len(path[i]) - 1):
                if (path[i][j + 1] == "+") or (path[i][j + 1] == "-") or (path[i][j + 1] == "X"):
                    return nevigate(path, i, j + 1, True, True)
    return False


def nevigate_validate(path):
    dire = True  #T- horizontal , F- vertical
    side = True  #T- right\down , F- left\up
    val= False
    for i in range(0,len(path)):
        for j in range(0, len(path[i])):
            if(path[i][j]=="X"):
                val = val or nevigate(path, i, j + 1, True, True) or nevigate(path, i, j - 1, True, False) or nevigate(path, i + 1, j, False, True) or nevigate(path, i - 1, j, False, False)
    return val


# small test to check it's work.
if __name__ == '__main__':
    path = ["                      ",
            "   +-------+          ",
            "   |      +++---+     ",
            "X--+      +-+   X     "]
    valid = nevigate_validate(path)

    path = ["           ",
            "X--|--+    ",
            "      -    ",
            "      +---X",
            "           "]
    invalid = nevigate_validate(path)
    if valid and not invalid:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")