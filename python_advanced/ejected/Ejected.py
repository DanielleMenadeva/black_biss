"""
Black Biss - Advance Python

Write a function that calculate how far is our ejected pilot.
Run this program and insert the following lines (each end with Enter):

UP 5
DOWN 3
LEFT 3
RIGHT 2
0

and the result should be 2
"""
import math


def direction(dire):
    if dire[0] == 'U':
        return (0, float(dire.split(" ")[1]))
    if dire[0] == 'D':
        return (0, -(float(dire.split(" ")[1])))
    if dire[0] == 'L':
        return (-float(dire.split(" ")[1]), 0)
    if dire[0] == 'R':
        return (float(dire.split(" ")[1]), 0)
    if dire[0] == '0':
        return tuple()


def distance(star, dest):
    return math.sqrt((dest[0] - star[0]) ** 2 + (dest[1] - star[1]) ** 2)


def ejected(black_box):
    black_box = black_box.split("\n")
    i = 1
    dire = (0, 0)
    temp = direction(black_box[0])
    while (temp) and (i < len(black_box)):
        dire = (temp[0] + dire[0], temp[1] + dire[1])
        temp = direction(black_box[i])
        i += 1
    return round(distance((0, 0), dire))


# small test to check it's work.
if __name__ == '__main__':
    black_box="UP 5\nDOWN 3\nLEFT 3\nRIGHT 2\n0"
    print(ejected(black_box))
