"""
Black Biss - Advance Python

Write a function that calculate how many month one should save money to buy new car.
"""


def home_economics(startPriceOld, startPriceNew, savingPerMonth, percentLossByMonth):
    month=0
    saved=0
    while startPriceNew > (startPriceOld + saved):
        month += 1
        if month%2==0:
            percentLossByMonth+=0.5
        startPriceOld -= (percentLossByMonth * startPriceOld) / 100.0
        startPriceNew -= (percentLossByMonth * startPriceNew) / 100.0
        saved+=savingPerMonth
    return (month, round(startPriceOld + saved - startPriceNew))


# small test to check it's work.
if __name__ == '__main__':
    ret = home_economics(2000, 8000, 1000, 1.5)
    print(ret)
    if ret[0] == 6 and ret[1] == 766:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")