"""
Black Biss - Advance Python

Write a function that suggest which word is the Pirates meant to write.
"""

from itertools import permutations


def dejumble(words, potentional_words):
    words = permutations(words)
    res= []
    for word in words:
        if(''.join(word) in potentional_words):
            res.append(''.join(word))
    return res



# small test to check it's work.
if __name__ == '__main__':

    ret = dejumble("orspt", ["sport", "parrot", "ports", "matey"])
    if len(ret) == 2 and set(ret) == set(["sport", "ports"]):
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")