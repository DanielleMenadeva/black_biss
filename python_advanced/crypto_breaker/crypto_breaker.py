"""
Black Biss - Advance Python

Write a function that get a path to file that encrypted with 3 english-small-letter xor key, and print the
deciphered text.
"""

def find_key(letters):
    # finding keys that could decode encoded characters into space and checking if keys are lowercase
    # dividing keys into sections and counting
    lst = [{}, {}, {}]
    for i in range(0, len(letters)):
        if (int(letters[i]) ^ 32) >= 97 and (int(letters[i]) ^ 32) <= 122:
            if int(letters[i]) ^ 32 not in lst[i % 3]:
                lst[i % 3][int(letters[i]) ^ 32] = 1
            else:
                lst[i % 3][int(letters[i]) ^ 32] = lst[i % 3].get(int(letters[i]) ^ 32) + 1
    keys=[]
    for key in lst:
        keys.append(sorted(key.items(), key=lambda x: x[1], reverse=True)[0][0])
    return keys


def crypto_breaker(file_path):
    text_file = open(file_path, 'r')
    letters = text_file.read().split(',')
    key = find_key(letters)
    lst=[]
    for i in range(0, len(letters), 3):
        lst.append(chr(int(letters[i]) ^ key[0]))
        lst.append(chr(int(letters[i + 1]) ^ key[1]))
        lst.append(chr(int(letters[i + 2]) ^ key[2]))
    return lst


# small test to check it's work.
if __name__ == '__main__':
    print(''.join(crypto_breaker('big_secret.txt')))