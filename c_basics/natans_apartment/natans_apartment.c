#include <string.h>
#include <stdlib.h>
#include <stdio.h>

void close_num(char* nums) {
    if (nums[0] == '0') {
        nums[1] = '8';
    }
    if (nums[0] == '1') {
        nums[1] = '2';
        nums[2] = '4';
    }
    if (nums[0] == '2') {
        nums[1] = '1';
        nums[2] = '3';
        nums[3] = '5';
    }
    if (nums[0] == '3') {
        nums[1] = '2';
        nums[2] = '6';
    }
    if (nums[0] == '4') {
        nums[1] = '1';
        nums[2] = '5';
        nums[3] = '7';
    }
    if (nums[0] == '5') {
        nums[1] = '2';
        nums[2] = '6';
        nums[3] = '4';
        nums[4] = '8';
    }
    if (nums[0] == '6') {
        nums[1] = '3';
        nums[2] = '5';
        nums[3] = '9';
    }
    if (nums[0] == '7') {
        nums[1] = '4';
        nums[2] = '8';
    }
    if (nums[0] == '8') {
        nums[1] = '5';
        nums[2] = '9';
        nums[3] = '7';
        nums[4] = '0';
    }
    if (nums[0] == '9') {
        nums[1] = '6';
        nums[2] = '8';
    }
}

void pass_gener(char* password, char* ptr, char* nums, unsigned int i) {
    
	if (i < strlen(password))
	{
        memset(nums, '\0', 6);
        nums[0] = password[i];
        close_num(nums);
        for (unsigned int j = 0; j < strlen(nums); j++)
        {
            nums[0] = password[i];
            close_num(nums);
            ptr[i] = nums[j];
            pass_gener(password, ptr, nums, i+1);
        }
	}
    if (i==strlen(password))
    {
        printf("%s\n", ptr);
    }
	
}

int main()
{
	char password[5] = "1357\0";
    char * ptr = (char*)malloc(sizeof(char) * (strlen(password)+1));
    memset(ptr, '\0', strlen(password) + 1);
    char nums[6];
    memset(nums, '\0', 6);

	pass_gener(password, ptr, nums, 0);
    free(ptr);
	return(0);
}